let xano = function(baseUrl) {
    this.apiKey = null;
    this.baseUrl = baseUrl;
    this.responseType = 'json';
};

xano.prototype.setApiKey = function(apiKey) {
    this.apiKey = apiKey;
    return this;
};

xano.prototype.setResponseType = function(responseType) {
    if (['text', 'json'].indexOf(responseType) === -1) {
        throw Error('Invalid responseType: ' + responseType);
    }

    this.responseType = responseType;
};

xano.prototype.get = function(path, paramsObj) {
    let url = new URL(this.baseUrl + path);

    if (paramsObj) {
        let params = new URLSearchParams(url.search);

        Object.keys(paramsObj).forEach((index) => {
            params.set(index, paramsObj[index]);
        });

        url.search = '?' + params.toString();
    }

    return this.request('GET', url.toString());
};

xano.prototype.post = function(path, paramsObj) {
    let url = new URL(this.baseUrl + path);

    let postParams = null;

    if (paramsObj) {
        let params = new URLSearchParams(url.search);

        Object.keys(paramsObj).forEach((index) => {
            params.set(index, paramsObj[index]);
        });

        postParams = params.toString();
    }

    return this.request('POST', url.toString(), postParams);
};

xano.prototype.request = function(method, url, postParams) {
    return new Promise((resolve, reject) => {
        const req = new XMLHttpRequest();

        req.responseType = this.responseType;

        req.addEventListener('load', function() {
            let resolver = reject;
            if (this.status >= 200 && this.status <= 299) {
                resolver = resolve;
            }

            resolver({
                'status': this.status,
                'response': this.responseType === 'json' ? this.response : this.responseText
            });
        });

        req.open(method, url);

        if (this.apiKey) {
            req.setRequestHeader('x-api-key', this.apiKey);
        }
        
        if (method === 'POST') {
            req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        }

        if (postParams) {
            req.send(postParams);
        } else {
            req.send();
        }
    });
};